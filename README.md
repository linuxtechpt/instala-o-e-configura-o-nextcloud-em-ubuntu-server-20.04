# Instalação e Configuração Nextcloud em Ubuntu Server 20.04

(fontes de ajuda)

*  https://www.digitalocean.com/community/tutorials/how-to-create-raid-arrays-with-mdadm-on-ubuntu-16-04
*  https://www.marksei.com/how-to-install-nextcloud-18-on-ubuntu/
*  https://docs.nextcloud.com/


**Configuração Base**



* Criar um array raid 1 

```
$ sudo su
$ apt install mdadm
$ mdadm --create --verbose /dev/md0 --level=1 --raid-devices=2 /dev/sdb /dev/sdc #colocar os vossos devices#

$ mkfs.ext4 -F /dev/md0
$ mkdir -p /mnt/md0/data
$ mount /dev/md0 /mnt/md0/data
$ mdadm --detail --scan | tee -a /etc/mdadm/mdadm.conf
$ update-initramfs -u
$ echo '/dev/md0 /mnt/md0 ext4 defaults,nofail,discard 0 0' | tee -a /etc/fstab
```


*** Instalar Ubuntu Server 20.04 LTS com Netplan **

```
$ sudo nano /etc/netplan/ficheiro.yaml (unico ficheiro yaml)

network:
    ethernets:
        ens18:
            addresses: [192.168.1.12/24]
            gateway4: 192.168.1.1
            dhcp4: no
            nameservers:
                addresses: [1.1.1.1]
    version: 2
    
$ sudo netplan apply
```


*** 1.1 - Update e upgrade ao sistema **

```
$ sudo apt update
$ sudo apt dist-upgrade
$ reboot
```


*** 2 - Instalar pacotes necessários **

```
$ apt-get install apache2 mariadb-server php7.4-gd php7.4-json php7.4-mysql php7.4-curl php7.4-mbstring php-mysql php-xml php-zip libapache2-mod-php php-gd php-json php-curl php-mbstring
apt-get install php7.4-intl php-imagick php7.4-xml php7.4-zip
```


*** 3 - Instalar base de dados (Mariadb) **

```
$ mysql -u root -p
$ CREATE DATABASE nextcloud;
$ CREATE USER 'linuxtech'@'localhost' IDENTIFIED BY 'Password';
$ GRANT ALL PRIVILEGES ON nextcloud.* TO 'linuxtech'@'localhost';
$ FLUSH PRIVILEGES;
$ ctrl + D
```


*** 4 - Instalar ultima versão nextcloud **

```
$ cd /var/www
$ sudo wget https://download.nextcloud.com/server...
$ sudo apt install unzip
$ sudo unzip nextcloud-18.0.4.zip
$ sudo rm nextcloud-18.0.4.zip
$ sudo chown -R www-data:www-data nextcloud/
```


*** 5 - Configurar o apache e criar um novo ficheiro nextcloud.conf **

```
$ sudo su
$ nano /etc/apache2/sites-available/nextcloud.conf

Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Require all granted
  AllowOverride All
  Options FollowSymLinks MultiViews

  <IfModule mod_dav.c>
    Dav off
  </IfModule>
  
  SetEnv HOME /var/www/nextcloud
  SetEnv HTTP_HOME /var/www/nextcloud

</Directory>


$ sudo su
$ a2ensite nextcloud
$ a2enmod rewrite headers env dir mime
# sed -i '/^memory_limit =/s/=.*/= 512M/' /etc/php/7.4/apache2/php.ini
$ systemctl restart apache2
$ reboot
```



*** 6 - Firewall **

```
$ sudo ufw allow http
$ sudo ufw allow https
```


Abrir site : ip/nextcloud 


**Mudar de armazenamento padrão para outro disco:**

```
$ sudo su
$ mkfs.ext4 -F /dev/sdb
$ mkdir -p /mnt/nextcloud/
$ mount /dev/sdb /mnt/nextcloud
$ echo '/dev/sdb /mnt/nextcloud ext4 defaults 0 0' | tee -a /etc/fstab
```


* reconfigurar a base de dados 

```
$ sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --on
$ mkdir -p /mnt/nextcloud/data/
$ cp -a /var/www/nextcloud/data/. /mnt/nextcloud/data/
$ chown -R www-data:www-data /mnt/nextcloud/
$ nano /var/www/nextcloud/config/config.php
```

    'datadirectory' => '/mnt/nextcloud/data/',
    
```
 $ mysql -uroot -p
 $ use nextcloud
 $ select * from oc_storages;
 $ update oc_storages set id='local::/mnt/nextcloud/data/' where id='local::/var/www/nextcloud/data/';
 $ quit;
 $ sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --off
 $ systemctl stop apache2
 $ reboot
```




